var client = new Faye.Client('https://api.cloud.dreamfactory.com:9292/bayeux');
var myDSP = 'https://dsp-go.cloud.dreamfactory.com/rest/db/todo/';
var qString = "?app_name=todoangular&fields=*";
var TodoCtrl = function ($scope, $http) {
    Scope = $scope;
    $scope.Messages = [];
    $scope.subscription = client.subscribe('/todo', function (message) {
        $scope.$apply(function () {
            switch (message.action) {
                case "add":
                    $scope.Todos.record.push(message.newData);
                    break;
                case "update":
                    updateByAttr($scope.Todos.record, 'id', message.newData.id, message.newData);
                    break;
                case "delete":
                    $("#row_" + message.newData.id).fadeOut();
                    break;

            }


        });


    });
    $http.get(myDSP + qString).success(function (data) {
        $scope.Todos = data;
    });
    $scope.addItem = function () {
        $scope.todo.complete = false;
        $http.post(myDSP + qString, $scope.todo).success(function (data) {
            client.publish('/todo', {action: "add", newData: data});
            $scope.todo = {};
        });

    }
    $scope.updateItem = function () {
        var todo = this.todo;

        if (this.todo.complete === false) {
            this.todo.complete = true;
        } else {
            this.todo.complete = false;
        }
        $('#item_' + todo.id).toggleClass('strike');
        $http.put(myDSP + qString, {id: todo.id}, todo).success(function (data) {
            client.publish('/todo', {action: "update", newData: data});


        });
    };
    $scope.deleteItem = function () {

        var id = this.todo.id;
        $http.delete(myDSP + id + qString).success(function (data) {
            client.publish('/todo', {action: "delete", newData: data});
        });
    }
    var updateByAttr = function (arr, attr1, value1, newRecord) {
        if (!arr) {
            return false;
        }
        var i = arr.length;
        while (i--) {
            if (arr[i] && arr[i][attr1] && (arguments.length > 2 && arr[i][attr1] === value1 )) {
                arr[i] = newRecord;
            }
        }
        return arr;
    };
};